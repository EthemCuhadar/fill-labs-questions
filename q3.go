package main

import "fmt"

// main is the entry point for the program.
func main() {
	// Print the string "Hello, world!" to stdout.
	println("Hello, world!")

	// Create a slice of strings.
	data := []interface{}{"apple", "pie", "apple", "red", "red", "red"}

	// Print the most repeated data in the slice.
	fmt.Println(mostRepeatedValue(data))

	// Create a slice of integers.
	data2 := []interface{}{1, 1, 2, 3, 3, 3, 5, 5, 6, 9, 7, 7, 7, 7, 7}

	// Print the most repeated data in the slice.
	fmt.Println(mostRepeatedValue(data2))

	// Create a slice of both strings and integers.
	data3 := []interface{}{"apple", "pie", "apple", "red", "red", "red", 1, 1, 2, 3, 3, 3, 5, 5, 6, 9}

	// Print the most repeated data in the slice.
	fmt.Println(mostRepeatedValue(data3))
}

// mostRepeatedValue returns the most repeated data in the given slice.
func mostRepeatedValue(data []interface{}) interface{} {
	// Create a map to store the data and its count.
	counts := make(map[interface{}]int)

	// Iterate over the data and count the number of times each data appears.
	for _, d := range data {
		counts[d]++
	}

	// Find the data with the most count.
	var max int
	var maxData interface{}
	for d, c := range counts {
		if c > max {
			max = c
			maxData = d
		}
	}

	return maxData
}

// addTwoNumbers adds two numbers together.
func AddTwoNumbers(a, b int) int {
	return a + b
}

// multiplyTwoNumbers multiplies two numbers together.
func MultiplyTwoNumbers(a, b int) int {
	return a * b
}
