package main

import "fmt"

// main is the entry point for the program.
func main() {
	// Print the string "Hello, world!" to stdout.
	println("Hello, world!")
	myFunc(9)
}

func myFunc(i int) {
	if i > 2 {
		myFunc(i / 2)
	}
	fmt.Println(i)
}
