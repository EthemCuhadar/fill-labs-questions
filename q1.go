package main

import (
	"fmt"
	"sort"
)

// main is the entry point for the program.
func main() {
	// Print the string "Hello, world!"
	println("Hello, world!")

	// Create a slice of strings.
	words := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}

	// Sort the words by the number of "a" in the word.
	sortWords(words)

	// Print the sorted slice.
	fmt.Println(words)
}

// sortWords sorts the words in the given slice by the number of characters "a"
// in the word decreasingly. If two words have the same number of "a" in them,
// then sort them by the length of the word decreasingly. After sorting, return
// the sorted slice.
func sortWords(words []string) []string {
	tmpMap := make(map[string]int)

	// Count the number of "a" in each word.
	for _, word := range words {
		tmpMap[word] = countA(word)
	}

	// Sort the words by the number of "a" in the word.
	sort.Slice(words, func(i, j int) bool {

		// If the number of "a" in the first word is greater than the number of "a" in the second word,
		if tmpMap[words[i]] == tmpMap[words[j]] {
			return len(words[i]) > len(words[j])
		}

		// If the number of "a" in the first word is less than the number of "a" in the second word,
		return tmpMap[words[i]] > tmpMap[words[j]]

	})

	// Return the sorted slice.
	return words
}

// countA returns the number of characters "a" in the given word.
func countA(word string) int {
	count := 0

	// Count the number of "a" in the word.
	for _, c := range word {
		if c == 'a' {
			count++
		}
	}
	return count
}
